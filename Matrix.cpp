#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include "Matrix.h"
using namespace std;

//конструкторы

Matrix::Matrix (int h, int w): n_rows(h), n_cols(w), f(true) {
	mat = new double*[h];
	for (int i = 0; i < h; ++i) {
		mat[i] = new double[w];
	}
}
Matrix::Matrix (double e = 0): n_rows(1), n_cols(1), f(true) {
	mat = new double*;
	*mat = new double;
	mat[0][0] = e;
}
Matrix::Matrix (double* p, int n): n_rows(1), n_cols(n), f(true) {
	mat = new double*;
	*mat = new double[n];
	for (int i = 0; i < n; ++i) {
		(*mat)[i] = p[i];
	}
}
Matrix::Matrix (int n, double* p): n_rows(n), n_cols(1), f(true) {
	mat = new double*[n];
	for (int i = 0; i < n; ++i) {
		mat[i] = new double;
		*mat[i] = p[i];
	}
}
Matrix::Matrix (const char *s): f(true) {
	n_rows = -1;
	n_cols = 1;
	for (int i = 0, f = 1; s[i] != '\0'; ++i) {
		if (s[i] == '{') { 
			++n_rows; 
		} else if (s[i] == '}') {
			f = 0;
		}	
		if (f && (s[i] == ',')) {
			++n_cols;
		}
	}
	mat = new double*[n_rows];
	for (int i = 0; i < n_rows; ++i) {
		mat[i] = new double[n_cols];
	}
	invalid_argument err("Неверный аргумент\n");
	int pntr = 0;
	char c;
	char buf[64];
	memset(buf, '\0', 64);
	//n-1 строк
	if ((c = s[pntr++]) != '{') {
		delete [] mat;
		throw err;
	}
	//n-1 элементов
	for (int i = 0; i < n_rows - 1; ++i) {
		if ((c = s[pntr++]) != '{') 
			throw err;
		for (int j = 0; j < n_cols - 1; ++j) {
			while ((c = s[pntr++]) != ',') {
				buf[strlen(buf)] = c;
			}
			mat[i][j] = atof(buf);
			memset(buf, '\0', 64);
			pntr++;
		}
		//n-й элемент
		while ((c = s[pntr++]) != '}') {
			buf[strlen(buf)] = c;
		}
		mat[i][n_cols - 1] = atof(buf);
		memset(buf, '\0', 64);
		pntr++;
		pntr++;
	}
	//n-я строка
	if ((c = s[pntr++]) != '{') 
		throw err;
	//n-1 элементов
	for (int j = 0; j < n_cols - 1; ++j) {
		while ((c = s[pntr++]) != ',') {
			buf[strlen(buf)] = c;
		}
		mat[n_rows - 1][j] = atof(buf);
		memset(buf, '\0', 64);
		pntr++;
	}
	//n-й элемент
	while ((c = s[pntr++]) != '}') {
		buf[strlen(buf)] = c;
	}
	mat[n_rows - 1][n_cols - 1] = atof(buf);
	memset(buf, '\0', 64);
}
Matrix::Matrix (Matrix & matrix) {
	n_rows = matrix.n_rows;
	n_cols = matrix.n_cols;
	f = matrix.f;
	mat = new double*[n_rows];
	for (int i = 0; i < n_rows; ++i) {
		mat[i] = new double[n_cols];
		for (int j = 0; j < n_cols; ++j) {
			mat[i][j] = matrix.mat[i][j];
		}
	}
}

//деструктор

Matrix::~Matrix () {
	for (int i = 0; i < n_rows; ++i) {
		delete [] mat[i];
	}
	delete [] mat;
}

//методы

Matrix Matrix::identity(int n) {
	Matrix I(n, n);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			if (i == j) 
				I.mat[i][j] = 1;
			else
				I.mat[i][j] = 0;
		}
	}
	return I;
}
Matrix Matrix::diagonal(double* val, int n) {
	Matrix D(n, n);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			if (i == j) 
				D.mat[i][j] = val[i];
			else
				D.mat[i][j] = 0;
		}
	}
	return D;
}
int Matrix::rows() const {
	return n_rows;
}
int Matrix::cols() const {
	return n_cols;
}
double Matrix::get(int i, int j) const {
	return mat[i][j];
}
void Matrix::set(int i, int j, double val) {
	mat[i][j] = val;
	return;
}

//перегрузка операций

Matrix Matrix::operator[] (int i) {
	if (f) {
		Matrix tmp(1, n_cols);
		tmp.f = false;
		if ((0 <= i) && (i < n_rows)) {
			for (int j = 0; j < n_cols; ++j) {
				tmp.mat[0][j] = mat[i][j];
			}
			return tmp;
		}
		range_error err("Выход за диапазон\n");
		throw err;
	} else {
		Matrix tmp(n_rows, 1);
		if ((0 <= i) && (i < n_cols)) {
			for (int j = 0; j < n_rows; ++j) {
				tmp.mat[j][0] = mat[j][i];
			}
			return tmp;
		}
		range_error err("Выход за диапазон\n");
		throw err;
	}
}

Matrix & Matrix::operator= (const Matrix & a) {
	if (*this == a) return *this;
	if ((a.n_rows == n_rows) && (a.n_cols == n_cols)) {
		for (int i = 0; i < a.n_rows; ++i) {
			for (int j = 0; j < a.n_cols; ++j) {
				mat[i][j] = a.mat[i][j];
			}
		}
		return *this;
	} else {
		range_error err("Матрицы разных размеров\n");
		throw err;
	}
}
Matrix & Matrix::operator+= (const Matrix & a) {
	if ((a.n_rows == n_rows) && (a.n_cols == n_cols)) {
		for (int i = 0; i < a.n_rows; ++i) {
			for (int j = 0; j < a.n_cols; ++j) {
				mat[i][j] += a.mat[i][j];
			}
		}
		return *this;
	} else {
		range_error err("Матрицы разных размеров\n");
		throw err;
	}
}
Matrix & Matrix::operator-= (const Matrix & a) {
	if ((a.n_rows == n_rows) && (a.n_cols == n_cols)) {
		for (int i = 0; i < a.n_rows; ++i) {
			for (int j = 0; j < a.n_cols; ++j) {
				mat[i][j] -= a.mat[i][j];
			}
		}
		return *this;
	} else {
		range_error err("Матрицы разных размеров\n");
		throw err;
	}
}
Matrix & Matrix::operator*= (const Matrix & a) {
	if ((a.n_rows == n_rows) && (a.n_cols == n_cols)) {
		for (int i = 0; i < a.n_rows; ++i) {
			for (int j = 0; j < a.n_cols; ++j) {
				mat[i][j] *= a.mat[i][j];
			}
		}
		return *this;
	} else {
		range_error err("Матрицы разных размеров\n");
		throw err;
	}
}
Matrix Matrix::operator+ (const Matrix & a) {
	if ((a.n_rows == n_rows) && (a.n_cols == n_cols)) {
		Matrix tmp(n_rows, n_cols);
		for (int i = 0; i < a.n_rows; ++i) {
			for (int j = 0; j < a.n_cols; ++j) {
				tmp.mat[i][j] =  mat[i][j] + a.mat[i][j];
			}
		}
		return tmp;
	} else {
		range_error err("Матрицы разных размеров\n");
		throw err;
	}
}
Matrix Matrix::operator- (const Matrix & a) {
	if ((a.n_rows == n_rows) && (a.n_cols == n_cols)) {
		Matrix tmp(n_rows, n_cols);
		for (int i = 0; i < a.n_rows; ++i) {
			for (int j = 0; j < a.n_cols; ++j) {
				tmp.mat[i][j] =  mat[i][j] - a.mat[i][j];
			}
		}
		return tmp;
	} else {
		range_error err("Матрицы разных размеров\n");
		throw err;
	}
}
Matrix Matrix::operator* (const Matrix & a) {
	if ((a.n_rows == n_rows) && (a.n_cols == n_cols)) {
		Matrix tmp(n_rows, n_cols);
		for (int i = 0; i < a.n_rows; ++i) {
			for (int j = 0; j < a.n_cols; ++j) {
				tmp.mat[i][j] =  mat[i][j] * a.mat[i][j];
			}
		}
		return tmp;
	} else {
		range_error err("Матрицы разных размеров\n");
		throw err;
	}
}
Matrix & Matrix::operator- () {
	for (int i = 0; i < n_rows; ++i) {
		for (int j = 0; j < n_cols; ++j) {
			mat[i][j] =  mat[i][j] - 1;
		}
	}
	return *this;
}

ostream & operator<< (ostream & os, const Matrix & matrix) {
	cout << '{';
	for (int i = 0; i < matrix.rows() - 1; ++i) {
		cout << '{';
		for (int j = 0; j < matrix.cols() - 1; ++j) {
			cout << matrix.get(i, j) << ", ";
		}
		cout << matrix.get(i, matrix.cols() - 1) << "}, ";
	}
	cout << '{';
	for (int j = 0; j < matrix.cols() - 1; ++j) {
		cout << matrix.get(matrix.rows() - 1, j) << ", ";
	}
	cout << matrix.get(matrix.rows() - 1, matrix.cols() - 1) << "}}";
	return os;
}

//бинарные операции

bool operator== (const Matrix & a, const Matrix & b) {
	if ((a.rows() == b.rows()) && (a.cols() == b.cols())) {
		bool tmp = true;
		for (int i = 0; i < a.rows() && tmp; ++i) {
			for (int j = 0; j < a.cols() && tmp; ++j) {
				if (b.get(i, j) != a.get(i, j)) {
					tmp = false;
				}
			}
		}
		return tmp;
	} else {
		range_error err("Матрицы разных размеров\n");
		throw err;
	}
}

bool operator!= (const Matrix & a, const Matrix & b) {
	return !(a == b);
}
