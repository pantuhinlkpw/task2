#include <iostream>
#include "Matrix.h"
using namespace std;

int main() {
    cout << "Конструкторы" << endl;

	Matrix a(5, 2);
	cout << a << endl;

	Matrix b(2);
	cout << b << endl;

	double arr[10];
	for (int i = 0; i < 10; ++i) {
        arr[i] = i;
	}
	Matrix c(arr, 5);
	cout << c << endl;

    Matrix d(4, arr);
	cout << d << endl;

    Matrix e("{{5, 5, 3, 5}, {3, 5, 7, 2}, {1, 3, 5, 8}, {5, 5, 3, 5}}");
    cout << e << endl;

    cout << "Интерфейс класса" << endl;
    cout << Matrix::identity(3) << endl;
    cout << Matrix::diagonal(arr, 3) << endl;
    
	cout << e[1][2] << endl;
	cout << e[2] << endl;
	e.set(0, 0, 1);
	cout << e << endl;
	e = e + Matrix::identity(4);
	cout << e << endl;
	e = e - Matrix::identity(4);
	cout << e << endl;
	e = e * Matrix::identity(4);
	cout << e << endl;
	e += Matrix::identity(4);
	cout << e << endl;
	e -= Matrix::identity(4);
	cout << e << endl;
	e *= Matrix::identity(4);
	cout << e << endl;
	return 0;
}
