PROG = Matrix

comp: Matrix.o Main.o
	g++ -g -Wall Main.o Matrix.o -o $(PROG)
Main.o: Main.cpp
	g++ -c -g -Wall Main.cpp -o Main.o
Matrix.o: Matrix.cpp
	g++ -c -g -Wall Matrix.cpp -o Matrix.o
run:
	./$(PROG)
clean:
	rm -f *.o $(PROG)