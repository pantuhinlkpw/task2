#ifndef __Matrix__
#define __Matrix__

class Matrix {
	int n_rows; //количество строк матрицы
	int n_cols; //количество столбцов матрицы
	double **mat; //указатель на двойной массив
	bool f; //флаг для двойной индексации
public: 

	//конструкторы

	Matrix (int h, int w);
	Matrix (double e);
	Matrix (double* p, int n);
	Matrix (int n, double* p);
	Matrix (const char *s);
	Matrix (Matrix & matrix);

	//деструктор

	~Matrix ();

	//методы

	static Matrix identity(int n);
	static Matrix diagonal(double* val, int n);
	int rows() const;
	int cols() const;
	double get(int i, int j) const;
	void set(int i, int j, double val);

	//перегрузка операций

	Matrix operator[] (int i);

	friend bool operator!= (const Matrix & a, const Matrix & b);
	friend bool operator== (const Matrix & a, const Matrix & b);
	
	Matrix & operator= (const Matrix & a);
	Matrix & operator+= (const Matrix & a);
	Matrix & operator-= (const Matrix & a);
	Matrix & operator*= (const Matrix & a);
	Matrix operator+ (const Matrix & a);
	Matrix operator- (const Matrix & a);
	Matrix operator* (const Matrix & a);
	Matrix & operator- ();

	friend std::ostream & operator<< (std::ostream & os, const Matrix & matrix);
};

#endif
